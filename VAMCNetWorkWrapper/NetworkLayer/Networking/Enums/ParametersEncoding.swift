//
//  ParametersEncoding.swift
//  VAMCNetWorkWrapper
//
//  Created by Vamsee on 20/03/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//

//ParametersEncoding is an enum responsible for setting encoding type.

enum ParametersEncoding {
    case url
    case json
}
