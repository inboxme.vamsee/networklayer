//
//  NetworkError.swift
//  VAMCNetWorkWrapper
//
//  Created by Vamsee on 20/03/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//

enum NetworkError {
    case unknown
    case noJSONData
}
