//
//  HTTPMethod.swift
//  VAMCNetWorkWrapper
//
//  Created by Vamsee on 20/03/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//

//HTTPMethod is an enum responsible for setting HTTP method of requests. URLRequest has property .httpMethod to set method String type.

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
