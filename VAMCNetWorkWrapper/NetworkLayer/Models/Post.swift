//
//  Post.swift
//  VAMCNetWorkWrapper
//
//  Created by Vamsee on 20/03/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//


import Foundation

struct Post: Codable {
    let id: Int
    let title: String
    let body: String
}
