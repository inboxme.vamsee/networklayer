//
//  ViewController.swift
//  VAMCNetWorkWrapper
//
//  Created by Vamsee on 20/03/2020.
//  Copyright © 2020 ProKarma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let sessionProvider = URLSessionProvider()

    override func viewDidLoad() {
        super.viewDidLoad()
        getPosts()
    }

    private func getPosts() {
        sessionProvider.request(type: [Post].self, service: PostService.all) { response in
            switch response {
            case let .success(posts):
                print(posts)
            case let .failure(error):
                print(error)
            }
        }
    }

}

